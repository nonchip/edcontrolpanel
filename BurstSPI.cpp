// adapted from https://os.mbed.com/questions/81244/Getting-NeoPixels-WS2812-working-on-an-S/

#include "BurstSPI.h"

void BurstSPI::fastWrite(uint16_t data)
{
    uint8_t*            pData = (uint8_t*) &data;
    SPI_HandleTypeDef*  hspi = &(_peripheral->spi.spi.handle);
 
    hspi->Init.Mode = SPI_MODE_MASTER;
    hspi->TxXferCount = sizeof(data);;
    SPI_1LINE_TX(hspi);
 
    /* Check if the SPI is already enabled */
    if ((hspi->Instance->CR1 & SPI_CR1_SPE) != SPI_CR1_SPE)
        __HAL_SPI_ENABLE(hspi); /* Enable SPI peripheral */
 
    while (hspi->TxXferCount > 0U)
    {
        /* Wait until TXE flag is set to send data */
        if (__HAL_SPI_GET_FLAG(hspi, SPI_FLAG_TXE))
        {
            if (hspi->Init.DataSize == SPI_DATASIZE_16BIT)
            {
                /* Transmit data in 16 Bit mode */
                hspi->Instance->DR = *((uint16_t*)pData);
                pData += sizeof(uint16_t);
            }
            else
            {
                /* Transmit data in 8 Bit mode */
                *((__IO uint8_t *) &hspi->Instance->DR) = (*pData);
                pData += sizeof(uint8_t);
            }
 
            hspi->TxXferCount--;
        }
    }
}
 
void BurstSPI::clearRX(void)
{
    //Check if the RX buffer is busy
    SPI_TypeDef*    spi = _peripheral->spi.spi.handle.Instance;
    //While busy, keep checking
    while (spi->SR & SPI_SR_BSY)
    {
        // Check RX buffer readable
        while ((spi->SR & SPI_SR_RXNE) == 0);
 
        int dummy = spi->DR;
    }
}
