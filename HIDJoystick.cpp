#include "HIDJoystick.hpp"
#include "usb_phy_api.h"

HIDJoystick::HIDJoystick(rx_event_t* rx_event):
    USBHID(get_usb_phy(), 64, 64, 0x16D0, 0x0E70, 0x0000), rx_event(rx_event)
{
    queue = mbed_event_queue();
    connect();
}

HIDJoystick::~HIDJoystick()
{
    deinit();
}

void HIDJoystick::update(uint8_t x, uint8_t y, uint32_t buttons){
    MBED_PACKED(struct) {
        uint8_t x;
        uint8_t y;
        uint32_t b;
    } data = {x,y,buttons};
    HID_REPORT out;
    out.length = sizeof(data);
    memcpy(out.data,&data,out.length);
    send(&out);
}

const uint8_t *HIDJoystick::report_desc() {
    static const uint8_t num_buttons = 28;
    static const uint8_t report_descriptor[] = {
        USAGE_PAGE(1),              0x01, // Generic Desktop
        USAGE(1),                   0x05, // Gamepad
        COLLECTION(1),              0x01, // Application
            COLLECTION(1),          0x00, //    Physical
                USAGE_PAGE(1),      0x01, //        Generic Desktop
                REPORT_COUNT(1),    0x02, //        2 axes
                REPORT_SIZE(1),     0x08, //        1 byte each
                USAGE(1),           0x30, //        X axis
                USAGE(1),           0x31, //        Y axis
                LOGICAL_MINIMUM(1), 0x00, //        min 00
                LOGICAL_MAXIMUM(1), 0xff, //        max ff
                INPUT(1),           0x02, //        Data, Var, Abs
                REPORT_COUNT(1),    num_buttons,
                REPORT_SIZE(1),     0x01, //        1 Bit per report
                USAGE_PAGE(1),      0x09, //        Button
                USAGE_MINIMUM(1),   0x01, //        Button 1
                USAGE_MAXIMUM(1),   num_buttons,
                LOGICAL_MINIMUM(1), 0x00, //        Off
                LOGICAL_MAXIMUM(1), 0x01, //        On
                INPUT(1),           0x02, //        Data, Var, Abs
            END_COLLECTION(0),
        END_COLLECTION(0),
    };
    reportLength = sizeof(report_descriptor);
    return report_descriptor;
}

const uint8_t *HIDJoystick::configuration_desc(uint8_t index)
{
    if (index != 0) {
        return NULL;
    }
    uint8_t configuration_descriptor_temp[] = {
        CONFIGURATION_DESCRIPTOR_LENGTH,    // bLength
        CONFIGURATION_DESCRIPTOR,           // bDescriptorType
        LSB(HIDJoystick_TOTAL_DESCRIPTOR_LENGTH),       // wTotalLength (LSB)
        MSB(HIDJoystick_TOTAL_DESCRIPTOR_LENGTH),       // wTotalLength (MSB)
        0x01,                               // bNumInterfaces
        HIDJoystick_DEFAULT_CONFIGURATION,              // bConfigurationValue
        0x00,                               // iConfiguration
        C_RESERVED,                           // bmAttributes
        C_POWER(500),                         // bMaxPower

        INTERFACE_DESCRIPTOR_LENGTH,        // bLength
        INTERFACE_DESCRIPTOR,               // bDescriptorType
        0x00,                               // bInterfaceNumber
        0x00,                               // bAlternateSetting
        0x02,                               // bNumEndpoints
        HID_CLASS,                          // bInterfaceClass
        HID_SUBCLASS_NONE,                  // bInterfaceSubClass
        HID_PROTOCOL_NONE,                 // bInterfaceProtocol
        0x00,                               // iInterface

        HID_DESCRIPTOR_LENGTH,              // bLength
        HID_DESCRIPTOR,                     // bDescriptorType
        LSB(HID_VERSION_1_11),              // bcdHID (LSB)
        MSB(HID_VERSION_1_11),              // bcdHID (MSB)
        0x00,                               // bCountryCode
        0x01,                               // bNumDescriptors
        REPORT_DESCRIPTOR,                  // bDescriptorType
        (uint8_t)(LSB(report_desc_length())), // wDescriptorLength (LSB)
        (uint8_t)(MSB(report_desc_length())), // wDescriptorLength (MSB)

        ENDPOINT_DESCRIPTOR_LENGTH,         // bLength
        ENDPOINT_DESCRIPTOR,                // bDescriptorType
        _int_in,                            // bEndpointAddress
        E_INTERRUPT,                        // bmAttributes
        LSB(MAX_HID_REPORT_SIZE),           // wMaxPacketSize (LSB)
        MSB(MAX_HID_REPORT_SIZE),           // wMaxPacketSize (MSB)
        1,                                  // bInterval (milliseconds)

        ENDPOINT_DESCRIPTOR_LENGTH,         // bLength
        ENDPOINT_DESCRIPTOR,                // bDescriptorType
        _int_out,                           // bEndpointAddress
        E_INTERRUPT,                        // bmAttributes
        LSB(MAX_HID_REPORT_SIZE),           // wMaxPacketSize (LSB)
        MSB(MAX_HID_REPORT_SIZE),           // wMaxPacketSize (MSB)
        1,                                  // bInterval (milliseconds)
    };
    MBED_ASSERT(sizeof(configuration_descriptor_temp) == sizeof(_configuration_descriptor));
    memcpy(_configuration_descriptor, configuration_descriptor_temp, sizeof(_configuration_descriptor));
    return _configuration_descriptor;
}

const uint8_t *HIDJoystick::string_imanufacturer_desc()
{
    static const uint8_t string_imanufacturer_descriptor[] = {
        22+2,
        STRING_DESCRIPTOR,
        '.', 0, 'd', 0, 'e', 0, '.', 0, 'n', 0, 'o', 0, 'n', 0, 'c', 0, 'h', 0, 'i', 0, 'p', 0,
    };
    return string_imanufacturer_descriptor;
}

const uint8_t *HIDJoystick::string_iproduct_desc()
{
    static const uint8_t string_iproduct_descriptor[] = {
        28+2,
        STRING_DESCRIPTOR,
        'E', 0, 'D', 0, 'C', 0, 'o', 0, 'n', 0, 't', 0, 'r', 0, 'o', 0, 'l', 0, 'P', 0, 'a', 0, 'n', 0, 'e', 0, 'l', 0
    };
    return string_iproduct_descriptor;
}

const uint8_t *HIDJoystick::string_iserial_desc()
{
    static const uint8_t string_iserial_descriptor[] = {
        10+2,
        STRING_DESCRIPTOR,
        '0', 0, '.', 0, '0', 0, '.', 0, '0', 0,
    };
    return string_iserial_descriptor;
}

void HIDJoystick::report_rx() {
    if(!read_nb(&report_from_pc)) return;
    rx_event->post(report_from_pc.length,report_from_pc.data);
}

void HIDJoystick::report_tx() {

}
