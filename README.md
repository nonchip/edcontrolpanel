![photo](photo.jpg)

# features

* 128x64 white-on-black oled
* 8x ws2812
* 6 pushbuttons
* 5 toggle switches
* 5 toggle-style (on)-off-(on) switches (=10 pushbuttons)
* analog joystick w/ pushbutton
* rotary encoder w/ click (= 3 pushbuttons)

total inputs:
* 2 axes
* 25 buttons

# pins

             ^usb-port^
    B12 mat-col-6           GND
    B13 mat-col-5           GND
    B14 mat-col-4           3V3
    B15 mat-col-3           RST
    A8  mat-col-2           B11
    A9  mat-col-1           B10 knob btn
    A10                     B1 knob dt
    A11 USB                 B0 knob clk
    A12 USB                 A7 neopixel
    A15                     A6 mat-row-4
    B3                      A5 mat-row-3
    B4                      A4 mat-row-2
    B5                      A3 mat-row-1
    B6 i2c scl              A2 joystick btn
    B7 i2c sda              A1 joystick X
    B8                      A0 joystick Y
    B9                      C15 !
    5V0                     C14 !
    GND                     C13 !
    3V3                     VBAT

* `USB`: reserved for USB internals
* `!`: "dangerous" pins

matrix:
* 4 rows X 6 cols = 24 inputs
* rows:
  * 6 pushbuttons
  * 5 switches
  * first 3 (on)-off-(on)s
  * last 2 (on)-off-(on)s
* 1 diode per switch on the row side (`row pin -->|-- switch pin`)


# BOM
* [1x STM32F103C8 "Blue Pill" board](https://www.amazon.de/gp/product/B06XC5MVSY)
* [16x rectifier diode](https://www.amazon.de/gp/product/B01LQKR17C)
* [1x 8-led WS2812 strip](https://www.amazon.de/gp/product/B08J2S5N8D/)
* [1x rotary encoder w/ click](https://www.amazon.de/gp/product/B07T3672VK/)
* [5x `on-off` toggle switch](https://www.amazon.de/gp/product/B07KK78RRX/)
* [5x `(on)-off-(on)` self-returning switch](https://www.amazon.de/gp/product/B07H5NX4G9/)
* [6x Pushbutton](https://www.amazon.de/gp/product/B07N1N1T7R/)
* [1x "PS2 joystick breakout"](https://www.amazon.de/gp/product/B01M74LRAR/)
* [1x 128x64 monochrome i2c oled](https://www.amazon.de/-/en/gp/product/B01L9GC470/)
