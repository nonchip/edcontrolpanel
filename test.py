import hid
import ctypes
from pprint import pprint
from time import sleep
import sys


devpath=None

for entry in hid.enumerate(0x16d0, 0x0e70):
    if entry['manufacturer_string'] == '.de.nonchip' and entry['product_string']=='EDControlPanel':
        devpath = entry['path']
        break

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        return (0, 0, 0)
    if pos < 85:
        return (255 - pos * 3, pos * 3, 0)
    if pos < 170:
        pos -= 85
        return (0, 255 - pos * 3, pos * 3)
    pos -= 170
    return (pos * 3, 0, 255 - pos * 3)

if not devpath:
    print("cannot find device!")
    sys.exit(1)
with hid.Device(path=devpath) as dev:
    dev.nonblocking=False
    dev.write(bytes([0x00,0x00])) # oled clear
    sleep(0.05)
    dev.write(bytes([0x01,0,0,0x00])) # oled locate
    sleep(0.05)
    dev.write(b'\x02ED Control Panel\0') # oled print
    sleep(0.05)
    dev.write(bytes([0x01,1,0,0x00])) # oled locate
    sleep(0.05)
    dev.write(b'\x02by CMDR Thapala\0') # oled print
    sleep(0.05)
    dev.write(bytes([0x01,2,0,0x00])) # oled locate
    sleep(0.05)
    dev.write(b'\x02nonchip#2308\0') # oled print
    sleep(0.05)
    t=0
    dev.write(bytes([0x20,0xff, 0x00,0x00]))
    sleep(0.05)
    while True:
        dev.write(bytes([0x10]+[ byte>>6 for i in range(8) for byte in wheel((i*8+t)%256) ]+[0x00])) # leds
        sleep(0.1)
        t+=10
    dev.close()
