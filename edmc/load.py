import plug
import hid
import ctypes
import sys
from time import sleep, time
import tkinter as tk

dev=None
app=None

def connect_dev():
    global dev
    if dev:
        return
    devpath=None
    for entry in hid.enumerate(0x16d0, 0x0e70):
        print(entry['manufacturer_string']+' '+entry['product_string'])
        if entry['manufacturer_string'] == '.de.nonchip' and entry['product_string']=='EDControlPanel':
            devpath = entry['path']
            print(b"FOUND: "+devpath)
            break
    if devpath:
        dev = hid.Device(path=devpath)
    if not dev:
        print("cannot find EDControlPanel!")
    if dev:
        dev.nonblocking=False

def plugin_app(parent: tk.Frame) -> tk.Frame:
    global app
    app = tk.Frame(parent)
    tick()
    return app


def plugin_start3(plugin_dir: str) -> str:
    oled_clear()
    oled_locate(0,0)
    oled_print("EDMC online!")
    hid_send(bytes([0x10]+leds))
    return "edcontrolpanel"

def hid_send(b):
    global dev
    connect_dev()
    if not dev:
        return
    sleep(0.05)
    dev.write(b+bytes([0x00]))

def oled_clear():
    hid_send(bytes([0x00]))

def oled_locate(x,y):
    hid_send(bytes([0x01,x,y]))

last_oled_print = time()
def oled_print(s):
    global last_oled_print
    hid_send(bytes([0x02])+bytes(s.encode('utf-8')))
    last_oled_print = time()

def toggle_cmd(enabled,state):
    hid_send(bytes([0x20,enabled,state]))

def arr2int(arr):
    return sum(bool(v)<<i for i, v in enumerate(arr[::-1]))

leds=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
last_leds_set=[0,0,0,0,0,0,0,0]
def led_set(led,r,g,b):
    global leds
    global last_leds_set
    leds[led*3]=r
    leds[led*3+1]=g
    leds[led*3+2]=b
    hid_send(bytes([0x10]+leds))
    last_leds_set[led]=time()

def tick():
    global last_leds_set
    global last_oled_print
    # cool down leds
    for i in range(len(last_leds_set)):
        if last_leds_set[i] < time()-5:
            led_set(i,int(leds[i*3]*0.75),int(leds[i*3+1]*0.75),int(leds[i*3+2]*0.75))
    if last_oled_print < time()-30:
        oled_clear()
        oled_locate(0,0)
    app.master.after(500, tick)


def dashboard_entry(cmdr, is_beta, entry):
    toggle_cmd(arr2int([True,True,True,True,True]),arr2int([
        entry['Flags'] & plug.FlagsCargoScoopDeployed,
        entry['Flags'] & plug.FlagsLandingGearDown,
        entry['Flags'] & plug.FlagsLightsOn,
        entry['Flags'] & plug.FlagsNightVision,
        not bool(entry['Flags'] & plug.FlagsAnalysisMode),
    ]))

def journal_entry(cmdr, is_beta, system, station, entry, state) -> None:
    if entry["event"]=="HullDamage" and entry["PlayerPilot"]:
        for i in range(0,7):
            dmg=i>=(1.0-entry["Health"])*8.1
            led_set(i,dmg and 20 or 0, 0,0)
