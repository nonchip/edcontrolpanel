#include "mbed.h"

class Matrix{
public:
    Matrix(const PinName r[4],const PinName c[6]):
        rows{ {r[0],0},{r[1],0},{r[2],0},{r[3],0} },
        cols{ {c[0],PullDown},{c[1],PullDown},{c[2],PullDown},{c[3],PullDown},{c[4],PullDown},{c[5],PullDown} } {}
    uint32_t read(){
        uint32_t values=0;
        for(int i=0;i<4;i++){
            rows[i].write(1);
            for(int j=0;j<6;j++){
                values <<= 1;
                values |= (cols[j].read() & 1);
            }
            rows[i].write(0);
        }
        return values;
    }
private:
    DigitalOut rows[4];
    DigitalIn cols[6];
};