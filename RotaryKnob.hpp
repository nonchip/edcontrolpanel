#include "mbed.h"
class RotaryKnob{
public:
    RotaryKnob(PinName clk, PinName dt, PinName sw): in_clk(clk,PullUp), in_dt(dt,PullUp), in_sw(sw,PullUp) {
        in_clk.rise(mbed::callback(this,&RotaryKnob::tick));
        in_clk.fall(mbed::callback(this,&RotaryKnob::tick));
    }
    bool cw(){
        bool out = was_cw;
        was_cw = false;
        return out;
    }
    bool ccw(){
        bool out = was_ccw;
        was_ccw = false;
        return out;
    }
    bool sw(){
        return !in_sw.read();
    }
private:
    InterruptIn in_clk;
    DigitalIn in_dt;
    DigitalIn in_sw;
    bool was_cw;
    bool was_ccw;
    int prev_code;
    void tick(){
        int a  = in_clk.read();
        int b  = in_dt.read();
        int code = (a << 1) | b;
        if(      (prev_code == 0x3 && code == 0x0) || (prev_code == 0x0 && code == 0x3)) {
            was_cw=true;
        }else if((prev_code == 0x2 && code == 0x1) || (prev_code == 0x1 && code == 0x2)) {
            was_ccw=true;
        }
        prev_code = code;
    }
};
