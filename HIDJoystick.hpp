#pragma once
#include "mbed.h"
#include "mbed_events.h"
#include "usb/USBHID.h"

#define HIDJoystick_DEFAULT_CONFIGURATION (1)
#define HIDJoystick_TOTAL_DESCRIPTOR_LENGTH ((1 * CONFIGURATION_DESCRIPTOR_LENGTH) \
                               + (1 * INTERFACE_DESCRIPTOR_LENGTH) \
                               + (1 * HID_DESCRIPTOR_LENGTH) \
                               + (2 * ENDPOINT_DESCRIPTOR_LENGTH))

class HIDJoystick: public USBHID {
public:
    typedef Event<void(uint8_t,uint8_t*)> rx_event_t;
    HIDJoystick(rx_event_t* rx_event);
    virtual ~HIDJoystick() override;
    void update(uint8_t x, uint8_t y, uint32_t buttons);
protected:
    virtual const uint8_t *report_desc() override;
    virtual const uint8_t *configuration_desc(uint8_t index) override;
    virtual const uint8_t *string_imanufacturer_desc() override;
    virtual const uint8_t *string_iproduct_desc() override;
    virtual const uint8_t *string_iserial_desc() override;
    virtual void report_rx() override;
    virtual void report_tx() override;
private:
    EventQueue *queue;
    rx_event_t* rx_event;

    uint8_t _configuration_descriptor[HIDJoystick_TOTAL_DESCRIPTOR_LENGTH];
    HID_REPORT report_from_pc = {
        .length = 0,
        .data = {0}
    };
};
