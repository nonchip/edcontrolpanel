#include "mbed.h"
#include "mbed_events.h"

#include "ssd1306.h"
#include "HIDJoystick.hpp"
#include "neopixel.h"
#include "AnalogJoystick.hpp"
#include "RotaryKnob.hpp"
#include "Matrix.hpp"
using namespace neopixel;

#define WAIT_TIME_MS 500 
DigitalOut led1(LED1);

DigitalIn pixels_pulldown(SPI_MOSI, PullDown);
PixelArray pixels(SPI_MOSI, BYTE_ORDER_GRB, PROTOCOL_800KHZ);

AnalogJoystick joy(A1,A0,A2);
RotaryKnob knob(B0,B1,B10);
const PinName matrix_rownames[]={A3,A4,A5,A6};
const PinName matrix_colnames[]={A9,A8,B15,B14,B13,B12};
Matrix buttons(matrix_rownames, matrix_colnames);


SSD1306 oled(I2C_SDA, I2C_SCL, 0x78);

void hid_rx_handler(uint8_t, uint8_t*);

EventQueue *queue = mbed_event_queue();
HIDJoystick::rx_event_t hid_rx_event(queue, hid_rx_handler);

HIDJoystick hidjoy(&hid_rx_event);

enum hid_rx_command{
    oled_clear     =0x00,
    oled_locate    =0x01,
    oled_print     =0x02,
    leds_set       =0x10,
    faketoggle_set =0x20,
};

Pixel pixelbuffer[8];

uint8_t faketoggle_en=0;
uint8_t faketoggle_st=0;

void hid_rx_handler(uint8_t length, uint8_t* data){
    uint8_t command = data[0];
    switch(command){
        case(oled_clear):
            oled.cls();
            oled.redraw();
            break;
        case(oled_locate):
            oled.locate(data[1],data[2]);
            break;
        case(oled_print):
            oled.printf("%s",&data[1]);
            oled.redraw();
            break;
        case(leds_set):
            for(int i=0;i<8;i++){
                pixelbuffer[i].red   = data[1+i*3];
                pixelbuffer[i].green = data[2+i*3];
                pixelbuffer[i].blue  = data[3+i*3];
            }
            ThisThread::sleep_for(5ms);
            pixels.update(pixelbuffer,8);
            break;
        case(faketoggle_set):
            faketoggle_en = data[1];
            faketoggle_st = data[2];
    }
}
//00000011111000000000000000000000
uint32_t faketoggle(uint32_t btn){
    uint32_t out = btn;
    const static uint8_t offs=13;
    const static uint8_t num=5;
    for(uint8_t i=0; i<num; i++){
        uint8_t bit=i+offs;
        if(((faketoggle_en>>i) & 1) == 0)
            continue;
        if(((faketoggle_st>>i)&1) == ((btn>>bit)&1)){
            out &= ~(1<<bit);
            continue;
        }
        out |= (1<<bit);
        faketoggle_st = (faketoggle_st & ~(1UL << i)) | (((btn>>bit)&1) << i);
    }
    return out;
}

void joy_updater(){
    hidjoy.update((1.0f-joy.x())*255,(1.0f-joy.y())*255,
        joy.sw()       << 0 |
        knob.sw()      << 1 |
        knob.cw()      << 2 |
        knob.ccw()     << 3 |
        faketoggle(buttons.read()) << 4
    );
}

int main()
{
    oled.speed(SSD1306::Medium);
    oled.init();
    oled.cls();
    oled.redraw();

    Event<void(void)> joy_updater_event = queue->event(&joy_updater);
    joy_updater_event.period(100ms);
    joy_updater_event.post();

    queue->dispatch_forever();
    system_reset();
}
