#include <stdint.h>
#include "mbed.h"
#include "neopixel.h"

namespace neopixel
{

PixelArray::PixelArray(PinName out, ByteOrder byte_order, Protocol protocol)
    : spi_(out, NC, A5), byte_order_(byte_order), protocol_(protocol)
{
    if (protocol_ == PROTOCOL_800KHZ) {
        // These constraints are easy to meet by splitting each bit into three and packing them into SPI packets.
        // short: 220ns..420ns,  long: 750ns..1600ns, gap: >220ns
        //       0123456789abcdef
        //  '0': 0001110000000000          gap: 333ns, mark:  333ns, space: 1111ns
        //  '1': 0001111111111000                      mark: 1111ns, space:  333ns, gap: 333ns
        spi_.frequency(9000000);  // 111.1ns per bit
        spi_.format(16);
    }
}

static inline void SendBit(BurstSPI& spi, bool bit)
{
    spi.write(bit?0x1FF8:0x1C00);
}

static inline void SendEightBits(BurstSPI& spi, uint8_t bits){
    for (int i = 128; i >= 1; i >>= 1) {
        SendBit(spi, bits&i);
    }
}

void PixelArray::send_pixel(Pixel& pixel)
{
    uint8_t byte0 = (byte_order_ == BYTE_ORDER_RGB) ? pixel.red : pixel.green;
    uint8_t byte1 = (byte_order_ == BYTE_ORDER_RGB) ? pixel.green : pixel.red;
    uint8_t byte2 = pixel.blue;

    if (protocol_ == PROTOCOL_800KHZ) {
        SendEightBits(spi_,byte0);
        SendEightBits(spi_,byte1);
        SendEightBits(spi_,byte2);
    }
}

void PixelArray::update(Pixel buffer[], uint32_t length)
{
    for (size_t i = 0; i < length; i++) {
        send_pixel(buffer[i]);
    }

    wait_us(latch_time_us_);
}

void PixelArray::update(PixelGenerator generator, uint32_t length, uintptr_t extra)
{
/*    for (size_t i = 0; i < length; i++) {
        Pixel out;
        generator(&out, i, extra);
        send_pixel(out);
    }*/
    Pixel buffer[length];
    for (size_t i = 0; i < length; i++) {
        generator(&buffer[i],i,extra);
    }
    update(buffer,length);
}

}