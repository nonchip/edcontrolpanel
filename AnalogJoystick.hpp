#include "mbed.h"

class AnalogJoystick{
public:
    AnalogJoystick(PinName x, PinName y, PinName sw): in_x(x), in_y(y), in_sw(sw,PullUp) {}
    bool sw(){ return !in_sw.read(); }
    float x(){ return in_x.read(); }
    float y(){ return in_y.read(); }
private:
    AnalogIn in_x;
    AnalogIn in_y;
    DigitalIn in_sw;
};
