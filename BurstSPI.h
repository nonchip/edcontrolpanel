// adapted from https://os.mbed.com/users/Sissors/code/BurstSPI/
#pragma once
#include "mbed.h"

class BurstSPI : public SPI
{
public:
    BurstSPI(PinName mosi, PinName miso, PinName sclk) : SPI(mosi, miso, sclk) {};
    void fastWrite(uint16_t data);
    void setFormat( void ) {
        format(_bits, _mode);
        frequency(_hz);
    }
    void clearRX( void );
};
